package com.devsuperior.dscatalog.dto;

import com.devsuperior.dscatalog.entities.Category;
import com.devsuperior.dscatalog.entities.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@NoArgsConstructor
public class ProductDTO implements Serializable {

  private final List<CategoryDTO> categories = new ArrayList<>();
  @Setter
  private Long id;
  @Setter
  private String name;
  @Setter
  private String description;
  @Setter
  private Double price;
  @Setter
  private String imgUrl;
  @Setter
  private Instant date;

  public ProductDTO(Long id, String name, String description, Double price, String imgUrl, Instant date) {
	this.id = id;
	this.name = name;
	this.description = description;
	this.price = price;
	this.imgUrl = imgUrl;
	this.date = date;
  }

  public ProductDTO(Product product) {
	id = product.getId();
	name = product.getName();
	description = product.getDescription();
	price = product.getPrice();
	imgUrl = product.getImgUrl();
	date = product.getDate();
  }

  public ProductDTO(Product product, Set<Category> categories) {
	this(product);
	categories.forEach(category -> this.categories.add(new CategoryDTO(category)));
  }


}
