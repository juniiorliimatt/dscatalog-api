package com.devsuperior.dscatalog.dto;

import com.devsuperior.dscatalog.entities.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class UserDTO implements Serializable {

  @Setter
  private Long id;
  @Setter
  private String firstName;
  @Setter
  private String lastName;
  @Setter
  private String email;

  private Set<RoleDTO> roles = new HashSet<>();

  public UserDTO(User user) {
	this.id = user.getId();
	this.firstName = user.getFirstName();
	this.lastName = user.getLastName();
	this.email = user.getEmail();
	user.getRoles().forEach(role -> this.roles.add(new RoleDTO(role)));
  }


}
