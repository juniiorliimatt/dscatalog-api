package com.devsuperior.dscatalog.dto;

import com.devsuperior.dscatalog.entities.Category;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDTO implements Serializable {

  private Long id;
  private String name;

  public CategoryDTO(Category category) {
    id = category.getId();
    name = category.getName();
  }
}
