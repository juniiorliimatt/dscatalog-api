package com.devsuperior.dscatalog.services;

import com.devsuperior.dscatalog.dto.RoleDTO;
import com.devsuperior.dscatalog.dto.UserDTO;
import com.devsuperior.dscatalog.dto.UserInsertDTO;
import com.devsuperior.dscatalog.dto.UserUpdateDTO;
import com.devsuperior.dscatalog.entities.Role;
import com.devsuperior.dscatalog.entities.User;
import com.devsuperior.dscatalog.repositories.RoleRepository;
import com.devsuperior.dscatalog.repositories.UserRepository;
import com.devsuperior.dscatalog.services.exceptions.DatabaseException;
import com.devsuperior.dscatalog.services.exceptions.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

  private static Logger logger = LoggerFactory.getLogger(UserService.class);

  private final UserRepository repository;
  private final RoleRepository roleRepository;
  private final PasswordEncoder passwordEncoder;

  @Transactional(readOnly = true)
  public Page<UserDTO> findAllPaged(Pageable page) {
	var list = repository.findAll(page);
	return list.map(UserDTO::new);
  }

  @Transactional(readOnly = true)
  public UserDTO findById(Long id) {
	var optional = repository.findById(id);
	var user = optional.orElseThrow(() -> new ResourceNotFoundException("User not found"));
	return new UserDTO(user);
  }

  @Transactional
  public UserDTO save(UserInsertDTO userDTO) {
	var user = new User();
	copyDtoToEntity(userDTO, user);
	var password = passwordEncoder.encode(userDTO.getPassword());
	user.setPassword(password);
	user = repository.save(user);
	return new UserDTO(user);
  }

  @Transactional
  public UserDTO update(Long id, UserUpdateDTO userDTO) {
	try {
	  var user = repository.getOne(id);
	  copyDtoToEntity(userDTO, user);
	  user = repository.save(user);
	  return new UserDTO(user);
	} catch(EntityNotFoundException e) {
	  throw new ResourceNotFoundException("Id not found " + id);
	}
  }

  public void delete(Long id) {
	try {
	  repository.deleteById(id);
	} catch(EmptyResultDataAccessException e) {
	  throw new ResourceNotFoundException("Id not found");
	} catch(DataIntegrityViolationException e) {
	  throw new DatabaseException("Integrity violation");
	}
  }

  private void copyDtoToEntity(UserDTO userDTO, User user) {

	user.setFirstName(userDTO.getFirstName());
	user.setLastName(userDTO.getLastName());
	user.setEmail(userDTO.getEmail());

	user.getRoles().clear();
	for(RoleDTO roleDTO : userDTO.getRoles()) {
	  Role role = roleRepository.getOne(roleDTO.getId());
	  user.getRoles().add(role);
	}
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	var user = repository.findByEmail(username);
	if(Objects.isNull(user)) {
	  logger.error("User not found: " + username);
	  throw new UsernameNotFoundException("Email not found");
	}
	logger.info("User found: " + username);
	return user;
  }

}
