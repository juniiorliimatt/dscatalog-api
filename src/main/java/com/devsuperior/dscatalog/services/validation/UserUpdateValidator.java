package com.devsuperior.dscatalog.services.validation;

import com.devsuperior.dscatalog.dto.UserUpdateDTO;
import com.devsuperior.dscatalog.repositories.UserRepository;
import com.devsuperior.dscatalog.resources.exceptions.FieldMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RequiredArgsConstructor
public class UserUpdateValidator implements ConstraintValidator<UserUpdateValid, UserUpdateDTO> {

  private final HttpServletRequest request;
  private final UserRepository repository;

  @Override
  public void initialize(UserUpdateValid ann) {
  }

  @Override
  public boolean isValid(UserUpdateDTO dto, ConstraintValidatorContext context) {

	List<FieldMessage> list = new ArrayList<>();

	@SuppressWarnings("unchecked")
	var uriVars = (Map<String, String>) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
	var userId = Long.parseLong(uriVars.get("id"));

	var user = repository.findByEmail(dto.getEmail());
	if(Objects.nonNull(user) && userId != user.getId()) {
	  list.add(new FieldMessage("Email", "Email já existe"));
	}

	for(FieldMessage e : list) {
	  context.disableDefaultConstraintViolation();
	  context.buildConstraintViolationWithTemplate(e.getMessage())
			 .addPropertyNode(e.getFieldName())
			 .addConstraintViolation();
	}
	return list.isEmpty();
  }

}
