package com.devsuperior.dscatalog.services.validation;

import com.devsuperior.dscatalog.dto.UserInsertDTO;
import com.devsuperior.dscatalog.repositories.UserRepository;
import com.devsuperior.dscatalog.resources.exceptions.FieldMessage;
import lombok.RequiredArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
public class UserInsertValidator implements ConstraintValidator<UserInsertValid, UserInsertDTO> {

  private final UserRepository repository;

  @Override
  public void initialize(UserInsertValid ann) {
  }

  @Override
  public boolean isValid(UserInsertDTO dto, ConstraintValidatorContext context) {

	List<FieldMessage> list = new ArrayList<>();

	var user = repository.findByEmail(dto.getEmail());
	if(Objects.nonNull(user)) {
	  list.add(new FieldMessage("Email", "Email já existe"));
	}

	for(FieldMessage e : list) {
	  context.disableDefaultConstraintViolation();
	  context.buildConstraintViolationWithTemplate(e.getMessage())
			 .addPropertyNode(e.getFieldName())
			 .addConstraintViolation();
	}
	return list.isEmpty();
  }

}
