package com.devsuperior.dscatalog.resources.exceptions;

import com.devsuperior.dscatalog.services.exceptions.DatabaseException;
import com.devsuperior.dscatalog.services.exceptions.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.time.Instant;

@ControllerAdvice
public class ResourceExceptionHandler {

  @ExceptionHandler(ResourceNotFoundException.class)
  public ResponseEntity<StandardError> entityNotFound(ResourceNotFoundException e, HttpServletRequest request) {
	var status = HttpStatus.NOT_FOUND;
	var error = StandardError.builder()
							 .timestamp(Instant.now())
							 .status(status.value())
							 .error("Resource not found")
							 .message(e.getMessage())
							 .path(request.getRequestURI())
							 .build();

	return ResponseEntity.status(status).body(error);
  }

  @ExceptionHandler(DatabaseException.class)
  public ResponseEntity<StandardError> databaseException(DatabaseException e, HttpServletRequest request) {
	var status = HttpStatus.BAD_REQUEST;
	var error = StandardError.builder()
							 .timestamp(Instant.now())
							 .status(status.value())
							 .error("Database exception")
							 .message(e.getMessage())
							 .path(request.getRequestURI())
							 .build();

	return ResponseEntity.status(status).body(error);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ValidationError> methodArgumentNotValidException(MethodArgumentNotValidException e, HttpServletRequest request) {
	ValidationError error = new ValidationError();
	error.setTimestamp(Instant.now());
	error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
	error.setError("Validation exception");
	error.setMessage(e.getMessage());
	error.setPath(request.getRequestURI());

	for(FieldError f : e.getBindingResult().getFieldErrors()) {
	  error.addError(f.getField(), f.getDefaultMessage());
	}

	return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(error);
  }

  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<StandardError> constraintViolationException(ConstraintViolationException e, HttpServletRequest request) {
	ValidationError error = new ValidationError();
	error.setTimestamp(Instant.now());
	error.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
	error.setError("Validation exception");
	error.setMessage(e.getMessage());
	error.setPath(request.getRequestURI());

	e.getConstraintViolations()
	 .forEach(message -> error.addError(message.getPropertyPath().toString(), message.getMessageTemplate()));

	return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(error);
  }

}
