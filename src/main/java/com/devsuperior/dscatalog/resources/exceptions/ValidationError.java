package com.devsuperior.dscatalog.resources.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ValidationError extends StandardError {

  private List<FieldMessage> errors = new ArrayList<>();

  public ValidationError(Instant timestamp, int status, String error, String message, String path) {
	super(timestamp, status, error, message, path);
  }

  public void addError(String fieldName, String message) {
	errors.add(new FieldMessage(fieldName, message));
  }

}
